package main

import (
	"fmt"
	"os"

	"github.com/go-yaml/yaml"
)

type DatabaseConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Name     string `yaml:"name"`
	Engine   string `yaml:"engine"`
}

func (c DatabaseConfig) Dialect() string {
	return c.Engine
}

func (c DatabaseConfig) ConnectionInfo() string {
	if c.Engine == "postgres" {
		if c.Password == "" {
			return fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable",
				c.Host, c.Port, c.User, c.Name)
		}

		return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			c.Host, c.Port, c.User, c.Password, c.Name)
	}

	if c.Engine == "sqlite" {
		return c.Name
	}

	return ""
}

func DefaultPostgresConfig() DatabaseConfig {
	return DatabaseConfig{
		Host:     "localhost",
		Port:     5432,
		User:     "postgres",
		Password: "1234",
		Name:     "lenslocked_dev",
		Engine:   "postgres",
	}
}

type Config struct {
	Port           int    `yaml:"port"`
	Env            string `yaml:"env"`
	Pepper         string `yaml:"pepper"`
	HMACKey        string `yaml:"hmac_key"`
	DatabaseConfig `yaml:"database"`
	ImagesDir      string `yaml:"images_dir"`
}

func (c Config) IsProd() bool {
	return c.Env == "prod"
}

func DefaultConfig() Config {
	return Config{
		Port:           3000,
		Env:            "dev",
		Pepper:         "lL7#53SQsqzS@aSTFO&K",
		HMACKey:        "secret-hmac-key",
		DatabaseConfig: DefaultPostgresConfig(),
		ImagesDir:      "images/",
	}
}

func LoadConfig(file *string, required bool) Config {
	f, err := os.Open(*file)
	if err != nil {
		if required {
			panic(err)
		}
		fmt.Println("Using default config values...")
		return DefaultConfig()
	}
	defer f.Close()
	var c Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&c)
	if err != nil {
		panic(err)
	}

	return c
}
