module lenslocked.com

go 1.15

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gorilla/csrf v1.7.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gorm.io/driver/postgres v1.1.2 // indirect
	gorm.io/driver/sqlite v1.1.5 // indirect
	gorm.io/gorm v1.21.15 // indirect
)
