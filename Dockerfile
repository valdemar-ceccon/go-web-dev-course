FROM golang:1.17.6-bullseye AS build-env
WORKDIR /server
COPY . .

RUN go get

RUN GOOS=linux GOARCH=amd64 go build -o server

RUN chmod +x server

FROM debian:bullseye-slim AS runtime

WORKDIR /app

COPY --from=build-env /server/server /app/server
COPY assets /app/assets
COPY views /app/views

EXPOSE 3000

ENTRYPOINT ["/app/server"]
