package models

import "strings"

const (
	ErrInvalidDialect dbError = "db: invalid select dialect"
	// ErrNotFound is returned when a resourse cannot be found
	// in the database
	ErrNotFound modelError = "models: resource not found"
	// ErrPasswordIncorrect is returned when a invalid password
	// is used when attempting to authenticate a user
	ErrPasswordIncorrect modelError = "models: incorrect password provided"
	// ErrIDInvalid is returned when a invalid id is provided to a method
	// like delete
	ErrEmailRequired modelError = "models: Email address is required"
	// ErrEmailInvalid is returned when a email does not match
	// userValidator.emailRegex
	ErrEmailInvalid modelError = "models: Email address is not valid"
	// ErrEmailTaken is returned when a email is already being used by another
	// user
	ErrEmailTaken modelError = "models: Email address is already taken"
	// ErrPasswordTooShort is returned when an update or create is atempted with a password
	// that is less that 8 characters
	ErrPasswordTooShort modelError = "models: Password must be at least 8 characters long"
	// ErrPasswordRequired is returned when a user atempt to create a password but
	// don't provide a password
	// ErrRememberRequired is returned when a user atempt to create or update without a remember
	ErrRememberRequired modelError = "models: Remember is required"
	ErrTitleRequired    modelError = "models: title is required"
	// ErrEmailRequired is returned when a email is not set
	ErrPasswordRequired modelError = "models: Password is required"

	ErrIDInvalid privateError = "models: ID provided was invalid"
	// ErrRememberTooShort is returned when a remember token is not at least 32 bytes
	ErrRememberTooShort privateError = "models: remember token must be at least 32 bytes long"
	ErrUserIDRequired   privateError = "models: user ID is required"
)

type modelError string
type dbError string

func (e modelError) Error() string {
	return string(e)
}

func (e dbError) Error() string {
	return string(e)
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}

func (e modelError) Public() string {
	s := strings.Replace(string(e), "models: ", "", 1)
	split := strings.Split(s, " ")
	split[0] = strings.Title(split[0])
	return strings.Join(split, " ")
}
