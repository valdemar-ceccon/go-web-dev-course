package middleware

import (
	"fmt"
	"github.com/gorilla/mux"
	"lenslocked.com/context"
	"lenslocked.com/models"
	"lenslocked.com/views"
	"net/http"
	"strconv"
	"strings"
)

type User struct {
	models.UserService
}

func (mw *User) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

func (mw *User) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		if strings.HasPrefix(path, "/assets/") ||
			strings.HasPrefix(path, "/images/") {
			next(w, r)
			return
		}
		cookie, err := r.Cookie("remember_token")
		if err != nil {
			next(w, r)
			return
		}

		user, err := mw.ByRemember(cookie.Value)
		if err != nil {
			next(w, r)
			return
		}
		ctx := r.Context()
		ctx = context.WithUser(ctx, user)
		r = r.WithContext(ctx)
		next(w, r)
	}
}

type RequireUser struct {
	User
}

func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFn(next.ServeHTTP)
}

func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return mw.User.ApplyFn(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		next(w, r)
	})
}

type MustOwn struct {
	RequireUser
	models.GalleryService
}

func (mo *MustOwn) Apply(next http.Handler) http.HandlerFunc {
	return mo.ApplyFn(next.ServeHTTP)
}

func (mo *MustOwn) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return mo.RequireUser.ApplyFn(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		galleryId, ok := mux.Vars(r)["id"]
		if !ok {
			next(w, r)
			return
		}
		gId, _ := strconv.Atoi(galleryId)
		gallery, err := mo.GalleryService.ByID(uint(gId))
		if err != nil {
			next(w,r)
			return
		}
		fmt.Printf("%v\n%v\n",gallery, user)
		if gallery.UserID != user.ID {

			alert := views.Alert{
				Level:views.AlertLvlWarning,
				Message: "Cant edit gallery from another user",
			}
			views.RedirectAlert(w,r, "/", http.StatusFound, alert)
			return
		}

		next(w, r)
	})
}
