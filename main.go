package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/csrf"
	"gorm.io/gorm/logger"
	"lenslocked.com/middleware"
	"lenslocked.com/rand"

	"lenslocked.com/controllers"
	"lenslocked.com/models"

	"github.com/gorilla/mux"
)

func main() {
	required := flag.Bool("prod", false, "Provide this flag in a production environment. This ensures that a config.yaml file is provided")
	configFile := flag.String("config", "config.yaml", "Configuration file location")
	flag.Parse()
	config := LoadConfig(configFile, *required)
	databaseConfig := config.DatabaseConfig

	services, err := models.NewServices(
		models.WithGorm(databaseConfig.Dialect(), databaseConfig.ConnectionInfo()),
		models.WithUser(config.Pepper, config.HMACKey),
		models.WithGallery(),
		models.WithImage(config.ImagesDir),
		models.WithLogMode(logger.Warn),
	)
	must(err)

	must(services.AutoMigrate())

	r := mux.NewRouter()
	staticController := controllers.NewStatic()
	usersController := controllers.NewUsers(services.User)
	galleriesController := controllers.NewGalleries(services.Gallery, services.User, services.Image, r)

	csrfToken, err := rand.Bytes(32)
	must(err)
	csrfMw := csrf.Protect(csrfToken, csrf.Secure(config.IsProd()))

	userMw := middleware.User{
		UserService: services.User,
	}
	requireUserMw := middleware.RequireUser{
		User: userMw,
	}
	ownerMw := middleware.MustOwn{
		RequireUser:    requireUserMw,
		GalleryService: services.Gallery,
	}

	_ = ownerMw

	r.Handle("/contact", staticController.Contact).Methods("GET")
	r.HandleFunc("/", galleriesController.AllGalleries).Methods("GET")
	r.HandleFunc("/signup", usersController.New).Methods("GET")
	r.HandleFunc("/signup", usersController.Create).Methods("POST")
	r.Handle("/login", usersController.LoginView).Methods("GET")
	r.HandleFunc("/login", usersController.Login).Methods("POST")
	r.HandleFunc("/logout", usersController.Logout).Methods("POST")

	assetHandler := http.FileServer(http.Dir("./assets/"))
	assetHandler = http.StripPrefix("/assets/", assetHandler)
	r.PathPrefix("/assets/").Handler(assetHandler)

	imageHandler := http.FileServer(http.Dir(config.ImagesDir))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))

	r.Handle("/galleries", requireUserMw.ApplyFn(galleriesController.Index)).Methods("GET")
	r.Handle("/galleries/new", requireUserMw.Apply(galleriesController.New)).Methods("GET")
	r.HandleFunc("/galleries", requireUserMw.ApplyFn(galleriesController.Create)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/edit", ownerMw.ApplyFn(galleriesController.Edit)).
		Methods("GET").
		Name(controllers.EditGallery)
	r.HandleFunc("/galleries/{id:[0-9]+}/update", ownerMw.ApplyFn(galleriesController.Update)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/delete", ownerMw.ApplyFn(galleriesController.Delete)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/images", ownerMw.ApplyFn(galleriesController.ImageUpload)).Methods("POST")
	r.HandleFunc("/galleries/{id:[0-9]+}/images/{filename}/delete", ownerMw.ApplyFn(galleriesController.ImageDelete)).Methods("POST")

	r.HandleFunc("/galleries/{id:[0-9]+}", galleriesController.Show).
		Methods("GET").
		Name(controllers.ShowGallery)
	fmt.Printf("Starting the server at %d\n", config.Port)
	must(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), csrfMw(userMw.Apply(r))))
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
